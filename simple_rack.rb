# Write a simple Rack application which when run, accepts a command-line argument.
# Display this command-line argument.
argument = ARGV.first
my_rack_proc = lambda { |env| [200, {"Content-Type" => "text/plain"}, ["Command line argument you typed was: #{argument}"]] }
puts my_rack_proc.call({})
